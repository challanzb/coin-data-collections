import string
from sqlalchemy import Column, Integer, create_engine,Column,Integer,String,create_engine,Float
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from time import sleep
import json
import requests as r


engine = create_engine("sqlite:///main.db",echo=False)

Session = sessionmaker(bind=engine)
session = Session()

Base=declarative_base()

class Coins(Base):
    __tablename__='coins'
    id=Column(Integer,primary_key=True)
    ids=Column(String(50))
    currencies=Column(String(50))
    price_change=Column(Integer)
    price_change_in_24h=Column(Float)
    price_change_in_1h=Column(Float)
    price_change_in_7d=Column(Float)

    def present_price(self):
        resp_1=r.get('https://api.coingecko.com/api/v3/coins',params={'id':self.ids}).json()       
        self.price_change=resp_1[0]['market_data']['current_price'][self.currencies]
        self.price_change_in_24h=resp_1[0]['market_data']["price_change_24h_in_currency"][self.currencies]
        self.price_change_in_1h=resp_1[0]['market_data']["price_change_percentage_1h_in_currency"][self.currencies]
        self.price_change_in_7d=resp_1[0]['market_data']["price_change_percentage_7d_in_currency"][self.currencies]






